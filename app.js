///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////
// // Single line commment

// /*
//     Multi
//     line
//     comment
// */

// Log to console
// console.log('Hello World');
// console.log(123);
// console.log(true);
// var greeting  = 'Hello';
// console.log(greeting);
// console.log([1, 3, 6, 9]);
// console.log({a:1, b:2});

// console.table({a:1, b:2});

// console.error('This is error');

// console.warn('This is a warning');

// console.time('Hello');

// console.log('Hello World');
// console.log('Hello World');
// console.log('Hello World');
// console.log('Hello World');
// console.log('Hello World');
// console.log('Hello World');

// console.timeEnd('Hello');
// console.clear();

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// var, let, const

// // VAR
// var name = 'John Doe';
// console.log(name);
// name = 'Steve Smith';
// console.log(name);

// // Init variable
// var greeting;
// console.log(greeting);
// greeting = 'Hello';
// console.log(greeting);

// // letters, numbers, _, $
// // Can't start with number

// // Multi word variables
// var firstName = 'John'; // Camel case
// var first_name = 'John'; // underscroe
// var FirstName = 'John'; // Pascal case

// // LET
// let name = 'John Doe';
// console.log(name);
// name = 'Steve Smith';
// console.log(name);

// let greeting;
// console.log(greeting);
// greeting = 'Hello';
// console.log(greeting);

// CONST
// const name = 'John Doe';
// console.log(name);
// // Can not reassign
// name = 'Steve Smith';
// console.log(name);
// // Have to assign a value
// const greeting;

// const person = {
//     name: 'John',
//     age: 32
// };

// person.name = 'Sara';
// person.age = 35;

// console.table(person);

// const numbers = [1, 3, 5, 7, 9];
// numbers.push(10);
// console.log(numbers);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Primitive data types

// // String
// const name = 'John Doe';
// console.log(typeof name);

// // Number
// const age  = 30;
// console.log(typeof age);

// // Boolean
// const hasKids  = false;
// console.log(typeof hasKids);

// // Null
// const car  = null;
// console.log(typeof car);

// // Undefined
// let test;
// console.log(typeof test);

// // Symbol
// const sym = Symbol();
// console.log(typeof sym);

// // Reference data types

// // Array
// const hobbies = ['movies', 'music'];
// console.log(typeof hobbies);

// // Object literals
// const address = {
//     city: 'Boston',
//     state: 'MA'
// };

// console.log(typeof address);

// // Date
// const today = new Date();
// console.log(today);
// console.log(typeof today);


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Type Conversion

// let val;

// Number to String
// val = String(555);
// val = String(4 + 4);

// // Boolean to String
// val = String(true);

// // Date to String
// val = String(new Date());

// // Array to String
// val = String([1, 2, 5, 9]);

// // toString()
// val = (5).toString();
// val = (true).toString();
// val = (new Date()).toString();
// val = ([1, 2, 5, 9]).toString();

// // String to number
// val = Number('5');
// val = Number(true);
// val = Number(false);
// val = Number(null);
// val = Number('Hello');
// val = Number([1, 2, 3]);

// // parstInt()

// val = parseInt('100');
// val = parseFloat('100.30');

// console.log(val);
// console.log(typeof val);
// console.log(val.length);
// console.log(val.toFixed(2));

// const val1 = '5';
// const val2 = 6;

// const sum = val1 + val2;
// console.log(sum);
// console.log(typeof sum);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Numbers and Math Object

// const num1 = 100;
// const num2 = 50;
// let val;

// // Simple math with numbers
// val = num1 + num2;
// val = num1 - num2;
// val = num1 / num2;
// val = num1 * num2;
// val = num1 % num2;

// // Math Object
// val = Math.PI;
// val = Math.E;
// val = Math.round(2.4);
// val = Math.ceil(2.4);
// val = Math.floor(2.8);
// val = Math.sqrt(64);
// val = Math.pow(2, 5);
// val = Math.abs(-3);
// val = Math.min(5, 9, 15, 78, -5);
// val = Math.max(5, 9, 15, 78);
// val = Math.random();
// val = Math.round(Math.random() * 10 + 1);

// console.log(val);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // String Methods and Cancatenation

// const firstName = 'William';
// const lastName = 'Johnson';
// const age = 32;
// const str = 'Hello there my name is brad';

// let val;

// val = firstName + lastName;

// // Concatenation
// val = firstName + ' ' + lastName;

// // Append
// val = 'Brad ';
// val += 'Traversy';
// val = 'Hello, my name is ' + firstName + ' and I am ' + age;
// val = `Hello, my name is ${firstName} and I am ${age}`;

// // Escaping
// val = 'That\'s awesome, I can\'t wait';

// // Length
// val = firstName.length;

// // concat
// val = firstName.concat(' ', lastName);

// // Change case
// val = firstName.toUpperCase();
// val = firstName.toLowerCase();
// val = firstName[0];

// // indexOf
// val = firstName.indexOf('l');
// val = firstName.lastIndexOf('l');

// // charAt
// val = firstName.charAt(2);

// // Get last character
// val = firstName.charAt(firstName.length - 1);

// // substring()
// val = firstName.substring(0, 4);

// // slice()
// val = firstName.slice(0, 4);
// val = firstName.slice(-3);

// // // split()
// val = str.split(' ');

// // replace()
// val = str.replace('Brad', 'Jack');

// // includes
// val = str.includes('Hello');

// console.log(val);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Template Literals

// const name = 'John';
// const age = 32;
// const job = 'Web Developer';
// const city = 'Miami';

// // Without template strings (es5)
// html = '<ul><li>Name: ' + name + '</li><li>Age: ' + age + ' </li><li>Job: ' + job + '</li><li>City: ' + city + '</li></ul>';

// html = '<ul>' + 
//         '<li>Name: ' + name + '</li>' +
//         '<li>Age: ' + age + ' </li>' +
//         '<li>Job: ' + job + '</li>' +
//         '<li>City: ' + city + '</li>' +
//         '</ul>';

// function hello() {
//     return 'Hello';
// }

// html = `<ul>
//             <li>Name: ${name}</li>
//             <li>Age:  ${age}</li>
//             <li>Job:  ${job}</li>
//             <li>City: ${city}</li>
//             <li>${2 + 2}</li>
//             <li>${hello()}</li>
//             <li>${age > 30 ? 'Over 30' : 'Under 30'}</li>
//         </ul>`;

// document.body.innerHTML = html;


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Arrays and Array Methods
// const number1 = [43, 56, 33, 23, 44, 36, 5];
// const number2 = new Array(22, 45, 33, 76, 54);
// const fruit = ['Banana', 'Apple', 'Pear', 'Orange'];
// const mixed = [22, 'Hello', true, undefined, null, {a:1, b:1}, new Date()];

// let val;

// // Get array length
// val = number1.length;

// // Check if is array
// val = Array.isArray(number1);

// // Get single value
// val = number1[3];
// val = number1[0];

// // Insert into array
// number1[2] = 100;

// // Find index of value
// val = number1.indexOf(36);

// // Mutating Arrays

// // Add on to end
// number1.push(250);

// // Add on to start
// number1.unshift(120);

// // Take of from end
// number1.pop();

// // Take of from start
// number1.shift();

// // Splice values
// number1.splice(1, 1);

// // Reverse
// number1.reverse();

// // Concatenate array
// val = number1.concat(number2);

// Sorting array
// val = fruit.sort();
// val = number1.sort();

// val = number1.sort(function (a, b) {
//     return a - b;
// });

// val = number1.sort(function (a, b) {
//     return b - a;
// });

// Find
// function under50(num) {
//     return num < 50;
// }

// val = number1.find(under50);

// console.log(number1);
// console.log(val);


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Object Literals

// const person = {
//     firstName: 'Steve',
//     lastName: 'Smith',
//     age: 30,
//     email: 'steve@gmail.com',
//     hobbies: ['music', 'sports'],
//     address: {
//         city: 'Miami',
//         state: 'FL'
//     },
//     getBirthYear: function () {
//         return 2017 - this.age;
//     }
// }

// let val;

// val = person;
// // Get specific value
// val = person.firstName;
// val = person['firstName']
// val = person.age;
// val = person.email;
// val = person.hobbies;
// val = person.hobbies[0];
// val = person.address;
// val = person.address.state;
// val = person.getBirthYear();

// console.log(val);

// const people = [
//     {name: 'John', age: 30},
//     {name: 'Mike', age: 23},
//     {name: 'Nancy', age: 40}
// ];

// for (let i = 0; i < people.length; i++) {
//     console.log(people[i].name);
// }


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Dates and Time

// let val;

// const today = new Date();
// let brithday = new Date('10-01-1989 00:50:00');
// brithday = new Date('10/01/1989');
// brithday = new Date('October 1 1989');

// val = today.getMonth();
// val = today.getDate();
// val = today.getDay();
// val = today.getFullYear();
// val = today.getHours();
// val = today.getMinutes();
// val = today.getSeconds();
// val = today.getMilliseconds();
// val = today.getTime();

// brithday.setMonth(5);
// brithday.setDate(07);
// brithday.setFullYear(1990);
// brithday.setHours(08);
// brithday.setMinutes(50);
// brithday.setSeconds(16);

// console.log(today);
// console.log(brithday);
// console.log(val);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // If statements and comparision operators

// const id = 100;

// // Equal to
// if (id == 100) {
//     console.log('CORRECT');
// } else {
//     console.log('INCORRECT');
// }

// // Not Equal to
// if (id != 101) {
//     console.log('CORRECT');
// } else {
//     console.log('INCORRECT');
// }

// // Equal To Value & Type
// if (id === 100) {
//     console.log('CORRECT');
// } else {
//     console.log('INCORRECT');
// }

// // Not Equal To Value & Type
// if (id !== 100) {
//     console.log('CORRECT');
// } else {
//     console.log('INCORRECT');
// }

// if (typeof id !== 'undefined') {
//     console.log(`The id is ${id}`);
// } else {
//     console.log('No id');
// }

// // Greater or less than
// if (id > 200) {
//     console.log('Correct');
// } else {
//     console.log('Incorrect');
// }

// // If else
// const color = 'yellow';

// if (color === 'red') {
//     console.log('Color is red.');
// } else if (color === 'blue') {
//     console.log('Color is blue.');
// } else {
//     console.log('Color is not red or blue.');
// }

// // Logical Operators

// const name = 'Steve';
// const age = 11;

// // AND &&
// if (age > 0 && age <= 12) {
//     console.log(`${name} is a child`);
// } else if (age >= 13 && age <= 19) {
//     console.log(`${name} is a teenager`);
// } else {
//     console.log(`${name} is a adult`);
// }

// // OR ||
// if (age < 16 || age > 65) {
//     console.log(`${name} can not run in race.`);
// } else {
//     console.log(`${name} is registered for the race.`);
// }

// // Ternary Operator
// console.log(id === 100 ? 'Correct' : 'Incorrect');

// // Without braces
// if (id === 100)
//     console.log('Correct');
// else
//  console.log('Incorrect');


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Switches

// const color = 'red';

// switch (color) {
//     case 'red':
//         console.log('Color is red');
//         break;
//     case 'blue':
//         console.log('Color is blue');
//         break;
//     default:
//         console.log('Color is not red or blue');
//         break;
// }

// switch (new Date().getDay()) {
//     case 0:
//         console.log('Sunday');
//         break;
//     case 1:
//         console.log('Monday');
//         break;    
//     case 2:
//         console.log('Tuesday');
//         break;
//     case 3:
//         console.log('Wednesday');
//         break;
//     case 4:
//         console.log('Thrusday');
//         break;
//     case 5:
//         console.log('Friday');
//         break;
//     case 6:
//         console.log('Saturday');
//         break;
// }


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Function Declaration

// function greet(firstName = 'John', lastName = 'Doe') {
    // console.log('Hello');
    // if (typeof firstName === 'undefined') {
    //     firstName = 'John';
    // }

    // if (typeof lastName === 'undefined') {
    //     lastName = 'Doe';
    // }

    // return `Hello ${firstName} ${lastName}`
// }

// console.log(greet('Brad', 'Traversy'));

// // Function Expression

// const square = function (x = 3) {
//     return x * x;
// }

// console.log(square());

// // Immediately Invokable Function Expression
// (function () {
//     console.log('IIFE Ran..');
// })();

// (function (firstName, lastName) {
//     console.log(`Hello ${firstName} ${lastName}`);
// })('Brad', 'Traversy');

// // Property Method
// const todo = {
//     add: function () {
//         console.log('Add todo..');
//     },
//     edit: function (id) {
//         console.log(`Edit todo.. ${id}`);
//     }
// };

// todo.delete = function (id) {
//     console.log(`Delete todo... ${id}`);
// }

// todo.add();
// todo.edit(22);
// todo.delete(45);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // General Loops

// // For loop
// for (let i = 0; i < 10; i++) {
//     if (i === 2) {
//         console.log(`${i} is my favorite number`);
//         continue;
//     }
//     if (i === 5) {
//         console.log('Stop Loop');
//         break;
//     }
//     console.log(i);
// }

// // While loop
// let i = 0;
// while (i < 10) {
//     console.log(i);
//     i++;
// }

// // Do while loop
// let i = 0;

// do {
//     console.log(i);
//     i++;
// } while (i < 10);

// const cars = ['ford', 'honda', 'toyota', 'Maruti Suzuki', 'Hyundai'];

// for (let i = 0; i < cars.length; i++) {
//     console.log(cars[i]);
// }

// // foreach
// cars.forEach((car, index, array) => {
//     console.log(`${index} : ${car}`);
//     // console.log(array);
// });

// // Map
// const users = [
//     {id: 1, name: 'John'},
//     {id: 2, name: 'Sara'},
//     {id: 3, name: 'Karen'}
// ];

// const ids = users.map(function (user) {
//     return user.id;
// })

// console.log(ids);

// const person = {
//     firstName: 'John',
//     lastName: 'Doe',
//     age: 32 
// };

// for (const key in person) {
    
//     console.log(`${key} : ${person[key]}`);
    
//     // if (person.hasOwnProperty(key)) {
//     //     console.log(person[key]);
//     // }
// }


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Window Methods / Objects / Properties

// Alert
// alert('hello');

// Prompt
// const input = prompt();
// console.log(input);

// Confirm
// if (confirm('Are you sure?')) {
//     console.log('OK');
// } else {
//     console.log('CANCEL');
// }

// let val;

// // Outer height and width
// val = window.outerHeight;
// val = window.outerWidth;

// // Inner height and width 
// val = window.innerHeight;
// val = window.innerWidth;

// // Scroll points
// val = window.scrollX;
// val = window.scrollY;

// // Location Object
// val = window.location;
// val = window.location.hostname;
// val = window.location.port;
// val = window.location.href;
// val = window.location.search;

// // Redirection
// window.location.href = 'https://www.google.co.in';

// // Reload
// window.location.reload();

// // History Object
// window.history.go(-2);
// val = window.history.length;

// // Navigator Object
// val = window.navigator;
// val = window.navigator.appName;
// val = window.navigator.appVersion;
// val = window.navigator.userAgent;
// val = window.navigator.platform;
// val = window.navigator.vendor;
// val = window.navigator.language;

// console.log(val);


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Block Scope let and const

// var a = 1;
// let b = 2;
// const c = 3;

// function test() {
//     var a = 4;
//     let b = 5;
//     const c = 6;
//     console.log('Function Scope', a, b, c);
// }

// test();

// if (true) {
//     var a = 4;
//     let b = 5;
//     const c = 6;
//     console.log('If Scope', a, b, c);
// }

// for (let a = 0; a < 10; a++) {
//     console.log(`Loop: ${a}`);
// }

// console.log('Global Scope', a, b, c);


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // Examining The Document Object
// let val;

// val = document;
// val = document.all;
// val = document.all[5];
// val = document.all.length;
// val = document.head;
// val = document.body;
// val = document.doctype;
// val = document.domain;
// val = document.URL;
// val = document.charset;
// val = document.contentType;

// val = document.forms;
// val = document.forms[0];
// val = document.forms[0].id;
// val = document.forms[0].method;
// val = document.forms[0].action;

// val = document.links;
// val = document.links[0];
// val = document.links[0].id;
// val = document.links[0].className;
// val = document.links[0].classList;

// val = document.images;

// val = document.scripts;
// val = document.scripts[1].getAttribute('src');

// let scripts = document.scripts;
// let scriptArr = Array.from(scripts);

// scriptArr.forEach(script => {
//     console.log(script.getAttribute('src'));
// });

// console.log(val);


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // document.getElementById();
// console.log(document.getElementById('task-title'));

// // Get things from element
// console.log(document.getElementById('task-title').id);
// console.log(document.getElementById('task-title').className);

// const taskTitle = document.getElementById('task-title');

// //Change styling
// taskTitle.style.background = '#333'
// taskTitle.style.color = '#fff'
// taskTitle.style.padding = '5px'
// // taskTitle.style.display = 'none'

// // Change content
// taskTitle.textContent = 'Task List';
// taskTitle.innerText = 'My Tasks';
// taskTitle.innerHTML = '<span style="color:red">Task List</span>';

// // document.querySelector()
// console.log(document.querySelector('#task-title'));
// console.log(document.querySelector('.card-title'));
// console.log(document.querySelector('h5'));
// document.querySelector('li').style.color = 'red';
// document.querySelector('ul li').style.color = 'blue';
// document.querySelector('li:last-child').style.color = 'red';
// document.querySelector('li:nth-child(3)').style.color = 'yellow';
// document.querySelector('li:nth-child(4)').textContent = 'Hello World';
// document.querySelector('li:nth-child(odd)').style.background = '#ccc';
// document.querySelector('li:nth-child(even)').style.background = '#f4f4f4';


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // document.getElementsByClassName()

// const items = document.getElementsByClassName('collection-item');

// console.log(items);
// console.log(items[0]);
// items[0].style.color = 'red';
// items[3].textContent = 'Hello';

// const listItems = document.querySelector('ul').getElementsByClassName('collection-item');
// console.log(listItems);

// // document.getElementsByTagName()

// let lis = document.getElementsByTagName('li');

// console.log(lis);
// console.log(lis[0]);
// lis[0].style.color = 'red';
// lis[3].textContent = 'Hello';

// lisArr = Array.from(lis);

// lisArr.reverse();

// // document.querySelectorAll()
// const items = document.querySelectorAll('li');

// items.forEach((item, index) => {
//     console.log(item);
//     item.textContent = `${index} : Hello`
// });

// console.log(items);

// const lisOdd = document.querySelectorAll('li:nth-child(odd)');
// const lisEven = document.querySelectorAll('li:nth-child(even)');

// lisOdd.forEach(function (li, index) {
//     li.style.background = '#ccc';
// })

// for (let i = 0; i < lisEven.length; i++) {
//     lisEven[i].style.background = '#f4f4f4';
    
// }

///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// let val;

// const list = document.querySelector('ul.collection');
// const listItem = document.querySelector('li.collection-item:first-child');

// val = listItem;
// val = list;

// // Get child node
// val = list.childNodes;
// val = list.childNodes[0];
// val = list.childNodes[0].nodeName;
// val = list.childNodes[1].nodeType;

// // 1 = Element
// // 2 = Attribute
// // 3 = Text node
// // 8 = Comment
// // 9 = Document itself
// // 10 = Docytype


// // Get children element nodes
// val = list.children;
// val = list.children[0];
// val = list.children[0].textContent = 'Hello';

// // Children of children
// val = list.children[3].children;

// // First child
// val = list.firstChild;
// val = list.firstElementChild;

// // last child
// val = list.lastChild;
// val = list.lastElementChild;

// // Count child elements
// val = list.childElementCount;

// // Get parent node
// val = listItem.parentNode;
// val = listItem.parentElement;
// val = listItem.parentElement.parentElement;

// // Get next sibling
// val = listItem.nextSibling;
// val = listItem.nextElementSibling.nextElementSibling.previousElementSibling;

// // Get pre sibling
// val = listItem.previousSibling;
// val = listItem.previousElementSibling;

// console.log(val);


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // Create Element
// const li = document.createElement('li');

// // Add class
// li.className = 'collection-item'

// // Add ID
// li.id = 'new-item'

// // Add Attribute
// li.setAttribute('title', 'New Item');

// // Create text node and append
// li.appendChild(document.createTextNode('Hello World'));

// // Create new link element
// const link = document.createElement('a');
// link.className = 'delete-item secondary-content';
// link.innerHTML = '<i class="fa fa-remove"></i>';
// li.appendChild(link);

// // Append li as child to ul
// document.querySelector('ul.collection').appendChild(li);

// console.log(li);


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // Replace element

// // Create element
// const newHeading = document.createElement('h2');
// // Add ID
// newHeading.id = 'task-title';

// // Create text node
// newHeading.appendChild(document.createTextNode('Task List'));

// // Get old heading
// const oldHeading = document.getElementById('task-title');
// // Parent
// const cardAction = document.querySelector('.card-action');

// // Replace
// cardAction.replaceChild(newHeading, oldHeading);

// // Remove element
// const lis = document.querySelectorAll('li');
// const list = document.querySelector('ul');

// // Remove list item
// lis[0].remove();

// // Remove child element
// list.removeChild(lis[3]);

// // Classes and attribute
// const firstli = document.querySelector('li:first-child');
// const link = firstli.children[0];

// let val;

// // Classes
// val = link.className;
// val = link.classList;
// val = link.classList[0];
// link.classList.add('test');
// link.classList.remove('test');
// val = link;

// // Attributes
// val = link.getAttribute('href');
// val = link.setAttribute('href', 'https://www.google.co.in');
// val = link.hasAttribute('href');
// val = link.hasAttribute('title');
// val = link;
// link.setAttribute('title', 'Google')
// link.removeAttribute('title');

// console.log(val);


///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// Event Listeners and Event Object

// document.querySelector('.clear-tasks').addEventListener('click', function (e) {
//     console.log('Hello World');
//     e.preventDefault();
// });

// document.querySelector('.clear-tasks').addEventListener('click', onClick);

// function onClick(e) {
//     // console.log('Clicked!!!');

//     let val;

//     val = e;

//     // Event target element 
//     val = e.target;
//     val = e.target.id;
//     val = e.target.className;
//     val = e.target.classList;
//     val = e.target.innerText;

//     // Event type
//     val = e.type;

//     // Timestamp
//     val = e.timeStamp;

//     // Coords event relative to window
//     val = e.clientY;
//     val = e.clientX;

//     // Coords event relative to element
//     val = e.offsetY;
//     val = e.offsetX;

//     console.log(val);

//     e.preventDefault();
// }

///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// // Mouse Event

// const clearBtn = document.querySelector('.clear-tasks');
// const card = document.querySelector('.card');
// const heading = document.querySelector('h5');

// // Click
// clearBtn.addEventListener('click', runEvent);

// // Double Click
// clearBtn.addEventListener('dblclick', runEvent);

// // Mousedown
// clearBtn.addEventListener('mousedown', runEvent);

// // Mouseup
// clearBtn.addEventListener('mouseup', runEvent);

// // Mouseenter
// card.addEventListener('mouseenter', runEvent);

// // Mouseleave
// card.addEventListener('mouseleave', runEvent);

// // Mouseover (works on internal element too)
// card.addEventListener('mouseover', runEvent);

// // Mouseout (works on internal element too)
// card.addEventListener('mouseout', runEvent);

// // Mouse move
// card.addEventListener('mousemove', runEvent);

// // Event Handler
// function runEvent(e) {
//     console.log(`Event Type: ${e.type}`);
//     heading.textContent = `MouseX: ${e.offsetX} MouseY: ${e.offsetY}`;
//     document.body.style.backgroundColor = `rgb(${e.offsetX}, ${e.offsetY}, 40)`
//     e.preventDefault();
// }

///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// const form = document.querySelector('form');
// const taskInput = document.getElementById('task');
// const heading = document.querySelector('h5');
// const select = document.querySelector('select');

// // clear input
// taskInput.value = '';
// form.addEventListener('submit', runEvent);

// // keydown
// // taskInput.addEventListener('keydown', runEvent);

// // keyup
// // taskInput.addEventListener('keyup', runEvent);

// // keypress
// // taskInput.addEventListener('keypress', runEvent);

// // focus
// // taskInput.addEventListener('focus', runEvent);

// // blur
// // taskInput.addEventListener('blur', runEvent);

// // copy
// // taskInput.addEventListener('copy', runEvent);

// // paste
// // taskInput.addEventListener('paste', runEvent);

// // cut
// // taskInput.addEventListener('cut', runEvent);

// // input
// // taskInput.addEventListener('input', runEvent);

// // change
// // select.addEventListener('change', runEvent);

// function runEvent(e) {
//     console.log(`Event Type: ${e.type}`);
//     // console.log(taskInput.value);
//     // console.log(select.value);
//     // heading.textContent = e.target.value;
//     // e.preventDefault();
// }

///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// Event Bubbling
// document.querySelector('.card-title').addEventListener('click', function () {
//     console.log('card title');
// });

// document.querySelector('.card-content').addEventListener('click', function () {
//     console.log('card content');
// });

// document.querySelector('.card').addEventListener('click', function () {
//     console.log('card');
// });

// document.querySelector('.col').addEventListener('click', function () {
//     console.log('col');
// });

// Event Delegation

// const delItem = document.querySelector('.delete-item');
// delItem.addEventListener('click', deleteItem);

// document.body.addEventListener('click', deleteItem)

// function deleteItem(e) {    
//     // if (e.target.parentElement.classname === 'delete-item secondary-conten') {
//     //     console.log('Delete Item');    
//     // }
    
//     if (e.target.parentElement.classList.contains('delete-item')) {
//         // console.log('Delete Item');
//         if (confirm('Are you sure?')) {
//             e.target.parentElement.parentElement.remove();   
//         }
//     }
// }

///////////////////////////////////////////////  2  /////////////////////////////////////////////////////////

// Set local storage item
// localStorage.setItem('name', 'John');
// localStorage.setItem('age', 32);

// Set session storage item
// sessionStorage.setItem('name', 'Beth');

// Remove from storage
// localStorage.removeItem('name');

// Get from local storage
// const name = localStorage.getItem('name');
// console.log(name);
// const age = localStorage.getItem('age');
// console.log(age);

// Clear storage
// localStorage.clear();

// document.querySelector('form').addEventListener('submit', function (e) {
//     const task = document.getElementById('task').value;
    
//     let tasks;

//     if (localStorage.getItem('tasks') === null) {
//         tasks = [];
//     } else {
//         tasks = JSON.parse(localStorage.getItem('tasks'));
//     }
    
//     tasks.push(task);
//     console.log(tasks);
//     localStorage.setItem('tasks', JSON.stringify(tasks));
//     e.preventDefault();
// })

// const tasks = JSON.parse(localStorage.getItem('tasks'));
// tasks.forEach(task => {
//     console.log(task);
// });

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Constructor and this keyword

// // Person Constructor
// function Person(name, dob) {
//     this.name = name;
//     // this.age = age;
//     this.birthday = new Date(dob);
//     this.calculateAge = function () {
//         const diff = Date.now() - this.birthday.getTime();
//         const ageDate = new Date(diff);
//         return Math.abs(ageDate.getUTCFullYear() - 1970);
//     }
// }

// // const brad = new Person('Brad', 36);
// // const john = new Person('John', 30);

// // console.log(brad);
// // console.log(john);

// const brad = new Person('Brad', '10-01-1989');

// console.log(brad.calculateAge());

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// String
// const name1 = 'Jeff';
// const name2 = new String('Jeff');
// // name2.foo = 'bar';
// console.log(name1);
// console.log(typeof name1);
// console.log(name2);
// console.log(typeof name2);

// if (name2 === 'Jeff') {
//     console.log('Yes');
// } else {
//     console.log('No');
// }

// Number
// const num1 = 5;
// const num2 = new Number(5);
// console.log(num1);
// console.log(typeof num1);
// console.log(num2);
// console.log(typeof num2);

// Boolean
// const bool1 = true;
// const bool2 = new Boolean(true);
// console.log(bool1);
// console.log(typeof bool1);
// console.log(bool2);
// console.log(typeof bool2);

// Function
// const getSum1 = function (x, y) {
//     return x + y;
// }

// const getSum2 = new Function('x', 'y', 'return x + y');

// console.log(getSum1(5, 6));
// console.log(getSum2(5, 6));

// Object
// const john1 = {
//     name: 'John'
// };

// const john2 = new Object({name: 'John'});
// console.log(john1);
// console.log(john2);

// Array
// const arr1 = [1, 4, 2, 6];
// const arr2 = new Array(1, 2, 3, 4);

// console.log(arr1);
// console.log(arr2);

// Regx
// const regx1 = /\w+/;
// const regx2 = new RegExp('\\w+');

// console.log(regx1);
// console.log(regx2);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Object.prototype

// // Person Constructor
// function Person(firstName, lastName, dob) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.birthday = new Date(dob);
//     // this.calculateAge = function () {
//     //     const diff = Date.now() - this.birthday.getTime();
//     //     const ageDate = new Date(diff);
//     //     return Math.abs(ageDate.getUTCFullYear() - 1970);
//     // }
// }

// // Calculate age
// Person.prototype.calculateAge = function () {
//     const diff = Date.now() - this.birthday.getTime();
//     const ageDate = new Date(diff);
//     return Math.abs(ageDate.getUTCFullYear() - 1970);
// }

// // Get fullname
// Person.prototype.getFullName = function () {
//     return `${this.firstName} ${this.lastName}`;
// }

// // Gets Married
// Person.prototype.getMarried = function (newLastName) {
//     this.lastName = newLastName;
// }

// const john = new Person('John', 'Doe', '10-1-1989');
// const mary = new Person('Mary', 'Johnson', '09-1-1997');

// console.log(mary);

// console.log(john.calculateAge());

// console.log(mary.getFullName());

// mary.getMarried('Smith');

// console.log(mary.getFullName());

// console.log(mary.hasOwnProperty('firstName'));

// console.log(mary.hasOwnProperty('getFullName'));

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Prototypal Inheritance

// // Person constructor
// function Person(firstName, lastName) {
//     this.firstName = firstName;
//     this.lastName = lastName;
// }

// // Greeting
// Person.prototype.greeting = function () {
//     return `Hello there ${this.firstName} ${this.lastName}`;
// }

// const person1 = new Person('John', 'Doe');
// // console.log(person1.greeting());

// // Customer constructor
// function Customer(firstName, lastName, phone, membership) {
//     Person.call(this, firstName, lastName);
//     this.phone = phone;
//     this.membership = membership;
// }

// // Inherit person prototype methods
// Customer.prototype = Object.create(Person.prototype);

// // Make Customer.prototype return Customer()
// Customer.prototype.constructor = Customer;

// // Create customer
// const customer1 = new Customer('Tom', 'Smith', '555-555-5555', 'standard');

// console.log(customer1);

// // customer greeting
// Customer.prototype.greeting = function () {
//     return `Hello there ${this.firstName} ${this.lastName} welcome to our company`;
// }

// console.log(customer1.greeting());

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// const personPrototypes = {
//     greeting: function () {
//         return `Hello there ${this.firstName} ${this.lastName}`;
//     },
//     getMarried: function (newlastName) {
//         this.lastName = newlastName;
//     }
// }

// const mary = Object.create(personPrototypes);
// mary.firstName = 'Mary';
// mary.lastName = 'Johnson';
// mary.age = 36;
// console.log(mary.greeting());
// mary.getMarried('Thompson');
// console.log(mary.greeting());

// const brad = Object.create(personPrototypes, {
//     firstName: {value: 'Brad'},
//     lastName: {value: 'Traversy'},
//     age: {value: 32}
// });

// console.log(brad);
// console.log(brad.greeting());

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // ES6 Classes
// class Person {
//     constructor(firstName, lastName, dob) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//         this.birthday = new Date(dob);
//     }

//     greeting() {
//         return `Hello there ${this.firstName} ${this.lastName}`;
//     }

//     calculateAge() {
//         const diff = Date.now() - this.birthday.getTime();
//         const ageDate = new Date(diff);
//         return Math.abs(ageDate.getUTCFullYear() - 1970)
//     }

//     getsMarried(newLastName) {
//         this.lastName = newLastName;
//     }

//     static addNumbers(x, y) {
//         return x + y;
//     }
// }

// const mary = new Person('Mary', 'Williams', '10-01-1989');
// console.log(mary.calculateAge());
// mary.getsMarried('Thompson');
// console.log(Person.addNumbers(5, 6));
// console.log(mary);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Sub Class

// class Person {
//     constructor(firstName, lastName) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//     }

//     greeting() {
//         return `Hello there ${this.firstName} ${this.lastName}`;
//     }
// }

// class Customer extends Person {
//     constructor(firstName, lastName, phone, membership) {
//         super(firstName, lastName);
//         this.phone = phone;
//         this.membership = membership;
//     }

//     static getMembershipCost() {
//         return 500;
//     }
// }

// const john = new Customer('John', 'Doe', '555-555-5555', 'Standard');

// console.log(john.greeting());
// console.log(Customer.getMembershipCost());

///////////////////////////////////////////////  3  /////////////////////////////////////////////////////////

// // XHR Object Methods And Working With Text
// document.querySelector('#button').addEventListener('click', loadDate);

// function loadDate() {
//     // Create an XHR Object
//     const xhr = new XMLHttpRequest();

//     // Open
//     xhr.open('GET', 'data.txt', true);

//     // xhr.onreadystatechange = function () {
//     //     if (this.status === 200 && this.readyState === 4) {
//     //         console.log(this.responseText);
//     //     }
//     // }    

//     // Optional - Used for spinners/loaders
//     xhr.onprogress = function () {
//         console.log(`READYSTATE ${xhr.readyState}`);
//     }

//     xhr.onload = function () {
//         console.log(`READYSTATE ${xhr.readyState}`);
//         if (this.status === 200) {
//             document.querySelector('#output').innerHTML = `<h1>${this.responseText}</h1>`
//             // console.log(this.responseText);   
//         }
//     }

//     xhr.onerror = function () {
//         console.log('There is error');
//     }

//     xhr.send();

//     // readyState Values
//     // 0: request not initialized
//     // 1: server connection established
//     // 2: request received
//     // 3: processing request
//     // 4: request finished and response is ready
// }


///////////////////////////////////////////////  4  /////////////////////////////////////////////////////////

// Working with ajax and json
// document.querySelector('#button1').addEventListener('click', loadCustomer);
// document.querySelector('#button2').addEventListener('click', loadCustomers);

// function loadCustomer(e) {
//     const xhr = new XMLHttpRequest();

//     xhr.open('GET', 'customer.json', true);

//     xhr.onload = function () {
//         if (this.status === 200) {
//             const customer = JSON.parse(this.responseText); 
//             document.querySelector('#customer').innerHTML = `
//                 <ul>
//                     <li>ID: ${customer.id}</li>
//                     <li>Name: ${customer.name}</li>
//                     <li>Company: ${customer.company}</li>
//                     <li>Phone: ${customer.phone}</li>
//                 </ul>
//             `;   
//         }
//     }

//     xhr.send();
// }

// function loadCustomers(e) {
//     const xhr = new XMLHttpRequest();

//     xhr.open('GET', 'customers.json', true);

//     xhr.onload = function () {
//         if (this.status === 200) {
//             const customers = JSON.parse(this.responseText);
//             let output = '';
//             customers.forEach(customer => {
//                 output += `
//                 <ul>
//                     <li>ID: ${customer.id}</li>
//                     <li>Name: ${customer.name}</li>
//                     <li>Company: ${customer.company}</li>
//                     <li>Phone: ${customer.phone}</li>
//                 </ul>
//                 `;
//             }); 
//             document.querySelector('#customers').innerHTML = output;   
//         }
//     }

//     xhr.send();
// }


///////////////////////////////////////////////  5  /////////////////////////////////////////////////////////

// // Chuck Norris Joke Generator

// document.querySelector('.get-jokes').addEventListener('click', getJokes);

// function getJokes(e) {
//     // const number = document.querySelector('#number').value;
//     const number = document.querySelector('input[type="number"]').value;

//     const xhr = new XMLHttpRequest();
    
//     xhr.open('GET', `http://api.icndb.com/jokes/random/${number}`, true);

//     xhr.onload = function () {
//         if (this.status === 200) {
//             const response = JSON.parse(this.responseText);
//             let output = '';
//             if (response.type === 'success') {
//                 response.value.forEach(joke => {
//                     output += `<li>${joke.joke}</li>`;
//                     // let li = document.createElement('li');
//                     // li.appendChild(document.createTextNode(joke.joke))
//                     // document.querySelector('.jokes').appendChild(li);
//                 });   
//             } else {
//                 output += '<li>Something went wrong</li>';
//             }
//             document.querySelector('.jokes').innerHTML = output;
//         }
//     }

//     xhr.send();

//     e.preventDefault();
// }


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Callback function
// const posts = [
//     {title: 'Post One', body: 'This is post one'},
//     {title: 'Post Two', body: 'This is post two'}
// ];

// function createPost(post) {
//     setTimeout(function () {
//         posts.push(post);
//     }, 2000);
// }

// function getPosts() {
//     setTimeout(function () {
//         let output = '';
//         posts.forEach(post => {
//             output += `<li>${post.title}</li>`;
//         });
//         document.body.innerHTML = output;
//     }, 1000);
// }

// createPost({title: 'Post Three', body: 'This is post three'});

// getPosts();

// function createPost(post, callback) {
//     setTimeout(function () {
//         posts.push(post);
//         callback();
//     }, 2000);
// }

// function getPosts() {
//     setTimeout(function () {
//         let output = '';
//         posts.forEach(post => {
//             output += `<li>${post.title}</li>`;
//         });
//         document.body.innerHTML = output;
//     }, 1000);
// }

// createPost({title: 'Post Three', body: 'This is post three'}, getPosts);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Promises

// const posts = [
//     {title: 'Post One', body: 'This is post one'},
//     {title: 'Post Two', body: 'This is post two'}
// ];

// function createPost(post) {    
//     return new Promise(function (resolve, reject) {
//         setTimeout(function () {
//             posts.push(post);
//             const err = false;
//             if (!err) {
//                 resolve();
//             } else {
//                 reject(`Error: Something went wrong`);
//             }
//         }, 2000); 
//     });
// }

// function getPosts() {
//     setTimeout(function () {
//         let output = '';
//         posts.forEach(post => {
//             output += `<li>${post.title}</li>`;
//         });
//         document.body.innerHTML = output;
//     }, 1000);
// }

// createPost({title: 'Post Three', body: 'This is post three'})
// .then(getPosts)
// .catch(function (err) {
//     console.log(err);
// });

///////////////////////////////////////////////  6  /////////////////////////////////////////////////////////

// // The Fetch API
// document.getElementById('button1').addEventListener('click', getText);
// document.getElementById('button2').addEventListener('click', getJson);
// document.getElementById('button3').addEventListener('click', getAPIData);

// // Get local text file data
// function getText() {
//     fetch('data.txt')
//         .then(function (res) {
//             return res.text();
//         })
//         .then(function (data) {
//             document.getElementById('output').innerHTML = data;
//         })
//         .catch(function (err) {
//             console.log(err);
//         });
// }

// // Get local json data
// function getJson() {
//     fetch('posts.json')
//     .then(function (res) {
//         return res.json();
//     })
//     .then(function (data) {
//         let output = '';
//         data.forEach(post => {
//             output += `<li>${post.title}</li>`;
//         });
//         document.getElementById('output').innerHTML = output;
//     })
//     .catch(function (err) {
//         console.log(err);
//     });
// }

// // Get data from externalAPI
// function getAPIData() {
//     fetch('https://api.github.com/users')
//     .then(function (res) {
//         return res.json();
//     })
//     .then(function (data) {
//         let output = '';
//         data.forEach(user => {
//             output += `<li>${user.login}</li>`;
//         });
//         document.getElementById('output').innerHTML = output;
//     })
//     .catch(function (err) {
//         console.log(err);
//     });
// }


///////////////////////////////////////////////  6  /////////////////////////////////////////////////////////

// Arrow Function
// const sayHello = function () {
//     console.log('Hello');
// }

// const sayHello = () => {
//     console.log('Hello');
// }

// One line function does not need {}
// const sayHello = () => console.log('Hello');

// One line return
// const sayHello = () => 'Hello';

// Same as above
// const sayHello = () => {
//     return 'Hello'
// }

// Return object
// const sayHello = () => ({msg: 'Hello'});

// Single param does not need ()
// const sayHello = name => console.log(`Hello ${name}`);

// Multiple param needs ()
// const sayHello = (firstName, lastName) => console.log(`Hello ${firstName} ${lastName}`);

// sayHello();
// sayHello('Brad', 'Traversy');

// const users = ['Nathan', 'John', 'William'];

// const nameLengths = users.map(function (name) {
//     return name.length;
// });

// const nameLengths = users.map(name => {
//     return name.length;
// });

// const nameLengths = users.map(name => name.length);

// console.log(nameLengths);

// document.getElementById('button1').addEventListener('click', getText);
// document.getElementById('button2').addEventListener('click', getJson);
// document.getElementById('button3').addEventListener('click', getAPIData);

// Get local text file data
// function getText() {
//     fetch('data.txt')
//     .then(res => res.text())
//     .then(data => document.getElementById('output').innerHTML = data)
//     .catch(err => console.log(err));
// }

// Get local json data
// function getJson() {
//     fetch('posts.json')
//     .then(res => res.json())
//     .then(data => {
//         let output = '';
//         data.forEach(post => {
//             output += `<li>${post.title}</li>`;
//         });
//         document.getElementById('output').innerHTML = output;
//     })
//     .catch(err => console.log(err));
// }

// Get data from externalAPI
// function getAPIData() {
//     fetch('https://api.github.com/users')
//     .then(res => res.json())
//     .then(data => {
//         let output = '';
//         data.forEach(user => {
//             output += `<li>${user.login}</li>`;
//         });
//         document.getElementById('output').innerHTML = output;
//     })
//     .catch(err => console.log(err));
// }

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Async and Await

// async function myFunc() {
//     // return 'Hello';
//     const promise = new Promise((resolve, reject) => {
//         setTimeout(() => resolve('Hello'), 5000);
//     });

//     const error = true;

//     if (!error) {
//         const res =  await promise;
//         return res;   
//     } else {
//         await Promise.reject(new Error('Something went wrong!!!'));
//     }
// }

// myFunc()
//     .then(res => console.log(res))
//     .catch(err => console.log(err))

// async function getUsers() {
//     const response = await fetch('https://jsonplaceholder.typicode.com/users');

//     const data = await response.json();

//     return data;
// }

// getUsers()
//     .then(users => console.log(users));


///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Error Handling Try and Catch

// const user = {email: 'jdoe@gmail.com'}; 

// try {
//     // Produce a ReferenceError
//     // myFunction();

//     // Produce a TypeError
//     // null.myFunction();

//     // Produce a SyntaxError
//     // console.log(eval('Hello World'));

//     // Produce a URIError
//     // decodeURIComponent('%');

//     if (!user.name) {
//         // throw 'User has no name.'
//         throw new SyntaxError('User has no name.');
//     }

// } catch (e) {
//     console.log(e);
//     // console.log(e.message);
//     // console.log(e.name);
//     // console.log(e instanceof ReferenceError);
//     // console.log(e instanceof TypeError);
// } finally {
//     console.log('Finally runs regardless of results..');
// }
// // myFunction();
// console.log('Program Continues....');

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Regular Expressions

// let re;
// re = /hello/;
// re = /hello/i; // i case insensitive
// re = /hello/g; // g global search all instances of 'hello'
// console.log(re);
// console.log(re.source);

// exec() -> will return result in array or null
// const result = re.exec('hello world');
// console.log(result);
// console.log(result[0]);
// console.log(result['index']);
// console.log(result['input']);

// test() -> return true or false
// const result = re.test('Hello world');
// console.log(result);

// match() -> return result array or null
// const str = 'Hello There';
// const result = str.match(re);
// console.log(result);

// search() -> return index of first match if not found -1
// const str = 'Hello There';
// const result = str.search(re);
// console.log(result);

// replace() -> return new string with some or all matches of pattern
// const str = 'Hello There Hello Hello xyz Hello';
// const newStr = str.replace(re, 'Hi');
// console.log(newStr);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// let re;

// // Literal Characters
// re = /hello/;
// re = /hello/i;

// // Meta Character Symbols
// re = /^h/i; // Must start with
// re = /world$/i; // Must end with
// re =  /^hello$/i; // Must start and end with
// re =  /h.llo/i; // Matches any one character
// re =  /h*llo/i; // Matches any character zero or more times
// re =  /gre?a?y/i; // Optional character
// re =  /gre?a?y\?/i; // Escape character

// // Brackets[] -> Character sets
// re =  /gr[ea]y/i; // Must be e or a
// re =  /[GF]ray/; // Must be G or F
// re =  /[^GF]ray/; // Matches anything except G or F
// re =  /[A-Z]ray/; // Match any uppercase letter
// re =  /[a-z]ray/; // Match any lowercase letter
// re =  /[A-Za-z]ray/; // Match any letter
// re =  /[0-9]ray/; // Match any digit

// // Braces {} - Quantifier
// re = /Hel{2}o/i; // Must occur exactly n number of times
// re = /Hel{2,4}o/i; // Must occur between 2 or 4 times
// re = /Hel{2,}o/i; // Must occur atleast 2 times

// // Parentheses () - Grouping
// re = /^([0-9]x){3}$/;

// // Shorthand character classes
// re = /\w/; // Word character - alphanumeric or _
// re = /\w+/; // + one or more
// re = /\W/; // Non word character
// re = /\d/; // Match any digit
// re = /\d+/; // Match any digit
// re = /\D/; // Match any non digit
// re = /\s/; // Match whitespace character
// re = /\S/; // Match non whitespace character
// re = /Hell\b/i; // Word boundary

// // Assertions
// re = /x(?=y)/; // Match x only if followed by y
// re = /x(?!y)/; // Match x only if not followed by y

// // String to match
// const str = 'dsfafsadfx';

// // Log Result
// const result = re.exec(str);
// console.log(result);

// function reTest(re, str) {
//     if (re.test(str)) {
//         console.log(`${str} matches ${re.source}`);
//     } else {
//         console.log(`${str} does not match ${re.source}`);
//     }
// }

// reTest(re, str);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Iterator Example

// function nameIterator(names) {
//     let nextIndex = 0;

//     return {
//         next: function () {
//             return nextIndex < names.length ?
//             { value: names[nextIndex++], done: false } :
//             { done: true }
//         }
//     }
// }

// // Create array of names
// const namesArr = ['Jack', 'John', 'Jill', 'Mary'];
// const names = nameIterator(namesArr);

// console.log(names.next());
// console.log(names.next());
// console.log(names.next());
// console.log(names.next());
// console.log(names.next());


// Generator Example (Note * after function)

// function* sayNames() {
//     yield 'Jack';
//     yield 'John';
//     yield 'Jill';
//     yield 'Mary';
// }

// const names = sayNames();

// console.log(names.next().value);
// console.log(names.next().value);
// console.log(names.next().value);
// console.log(names.next().value);
// console.log(names.next().value);

// function* createIds() {
//     let index = 1;

//     while(true) {
//         yield index++;
//     }
// }

// const gen = createIds();

// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);
// console.log(gen.next().value);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Symbol
// const sym1 = Symbol();
// const sym2 = Symbol('sym2');
// console.log(typeof sym2);
// console.log(Symbol() === Symbol());

// Unique object key
// const key1 = Symbol();
// const key2 = Symbol('sym2');

// const myObj = {};

// myObj[key1] = 'Prop1';
// myObj[key2] = 'Prop2';
// myObj.key3 = 'Prop3';
// myObj.key4 = 'Prop4';

// console.log(myObj);

// Symbols are not numerable in forin
// for (const key in myObj) {
//     console.log(`${key} : ${myObj[key]}`);
// }

// Symbols are ignored in JSON.stringify
// console.log(JSON.stringify(myObj));

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Destructuring Assignment
// let a, b;
// [a, b] = [100, 200];
// console.log(a, b);
// // Rest Pattern
// [a, b, c, ...rest] = [100, 200, 300, 400, 500];
// console.log(rest);

// ({ a, b } = { a:100, b:200, c:300, d:400, e:500});
// ({ a, b, ...rest } = { a:100, b:200, c:300, d:400, e:500});
// console.log(rest);

// Array Destructuring
// const people = ['John', 'Jill', 'Jack'];
// const [person1, person2, person3] = people;
// console.log(person1, person2, person3);

// Parse Array return from function
// function getPeople() {
//     return ['John', 'Jill', 'Jack'];
// }

// let person1, person2, person3;
// [person1, person2, person3] = getPeople();
// console.log(person1, person2, person3);

// Object Destructuring
// const person = {
//     name: 'John',
//     age: 32,
//     city: 'Miami',
//     gender: 'Male'
// };

// Old ES%
// var name = person.name,
//     age = person.age,
//     city = person.city;

// New ES6
// const { name, age, city } = person;
// console.log(name, age, city);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// Maps -> Key value pair of any type

// const map1 = new Map();

// // Set keys
// const key1 = 'String';
// const key2 = {};
// const key3 = function() {};
// const key4 = [];

// map1.set(key1, 'Value of key1')
// map1.set(key2, 'Value of key2')
// map1.set(key3, 'Value of key3')
// map1.set(key4, 'Value of key4')

// Get values by key
// console.log(map1.get(key1), map1.get(key2), map1.get(key3), map1.get(key4));

// Count values
// console.log(map1.size);

// Iterating through maps
// for (const [key, value] of map1) {
//     console.log(`${key} = ${value}`);
// }

// for (const key of map1.keys()) {
//     console.log(key);
// }

// for (const value of map1.values()) {
//     console.log(value);
// }

// map1.forEach((value, key) => {
//     console.log(`${key} = ${value}`);
// });

// Convert Maps to array
// const map1KeyValArr = Array.from(map1);
// console.log(map1KeyValArr);

// const map1ValArr = Array.from(map1.values());
// console.log(map1ValArr);

// const map1KeyArr = Array.from(map1.keys());
// console.log(map1KeyArr);

///////////////////////////////////////////////  1  /////////////////////////////////////////////////////////

// // Sets -> store unique value of any type
// const set1 = new Set();

// // Add value to set
// set1.add(100);
// set1.add('A String');
// set1.add({name: 'John'});
// set1.add(true);
// set1.add(100);
// // console.log(set1);

// const set2 = new Set([1, true, 'string']);
// // console.log(set2);

// // Get Count
// // console.log(set1.size);

// // Check Values
// // console.log(set1.has(100));
// // console.log(set1.has(50 + 50));
// // console.log(set1.has({name: 'John'})); // Reference type

// // Delete from set
// set1.delete(100);
// // console.log(set1);

// // Iterating through sets
// // for (const value of set1) {
// //     console.log(value);
// // }

// // Foreach loop
// // set1.forEach(value => {
// //     console.log(value);
// // });

// // Convert sets into array
// const set1Arr = Array.from(set1);
// console.log(set1Arr);